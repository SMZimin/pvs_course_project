#ifndef SMTP_REGEXP_TEST_H
#define SMTP_REGEXP_TEST_H


void get_email_from_str_usual_test1();
void get_email_from_str_usual_test2();
void get_email_from_str_usual_test3();
void get_email_from_str_usual_test4();
void get_email_from_str_usual_test5();
void get_email_from_str_incorrect_test1();
void get_email_from_str_incorrect_test2();
void get_email_from_str_incorrect_test3();
void get_email_from_str_incorrect_test4();
void get_email_from_str_incorrect_test5();

void get_domain_from_str_usual_test1();

void ehlo_usual_test();
void ehlo_down_case_test();
void ehlo_incorrect_test();

#endif // SMTP_REGEXP_TEST_H
