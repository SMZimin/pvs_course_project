#include "smtp_regexp_test.h"

#include <CUnit/CUnit.h>
#include <pcre.h>

#include "../../src/regexp_functions.h"
#include "../../src/memory_manager.h"

void get_email_from_str_usual_test1()
{
    const char *input_str = "MAIL FROM: <ya.zimin_sergey@yandex.ru>";
    const char *expected_str = "ya.zimin_sergey@yandex.ru";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void get_email_from_str_usual_test2()
{
    const char *input_str = "MAIL FROM: <ya.zimin_sergey@yandex.ru>\n";
    const char *expected_str = "ya.zimin_sergey@yandex.ru";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void get_email_from_str_usual_test3()
{
    const char *input_str = "MAIL FROM: <yazimin98sergey@yandex.ru>";
    const char *expected_str = "yazimin98sergey@yandex.ru";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void get_email_from_str_usual_test4()
{
    const char *input_str = "MAIL FROM: <s@ya.ru>";
    const char *expected_str = "s@ya.ru";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void get_email_from_str_usual_test5()
{
    const char *input_str = "MAIL FROM: <yazi.b8min-98sergey@ya8ndex.com>";
    const char *expected_str = "yazi.b8min-98sergey@ya8ndex.com";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void get_email_from_str_incorrect_test1()
{
    const char *input_str = "MAIL FROM: <@yandex.ru>";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NULL(output_str);
    free_all_generations();
}

void get_email_from_str_incorrect_test2()
{
    const char *input_str = "MAIL FROM: <34dsa@ya.ru>";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NULL(output_str);
    free_all_generations();
}

void get_email_from_str_incorrect_test3()
{
    const char *input_str = "MAIL FROM: <themail@yandex>";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NULL(output_str);
    free_all_generations();
}

void get_email_from_str_incorrect_test4()
{
    const char *input_str = "MAIL FROM: <themail@>";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NULL(output_str);
    free_all_generations();
}

void get_email_from_str_incorrect_test5()
{
    const char *input_str = "MAIL FROM: <themail.yandex.ru>";

    compile_regexps();
    char *output_str = get_email_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NULL(output_str);
    free_all_generations();
}

void get_domain_from_str_usual_test1()
{
    const char *input_str = "EHLO [127.0.0.1]";
    const char *expected_str = "127.0.0.1";

    compile_regexps();
    char *output_str = get_domain_from_str(input_str);
    dispose_regexps();

    CU_ASSERT_PTR_NOT_NULL_FATAL(output_str);
    CU_ASSERT_NSTRING_EQUAL(expected_str, output_str, strlen(expected_str) + 1);
    free_all_generations();
}

void ehlo_usual_test()
{
    const char *input_str = "EHLO [127.0.0.1]";

    compile_regexps();
    int result = is_ehlo_command(input_str);
    dispose_regexps();

    CU_ASSERT_TRUE(result);
    free_all_generations();
}

void ehlo_down_case_test()
{
    const char *input_str = "ehlo [127.0.0.1]";

    compile_regexps();
    int result = is_ehlo_command(input_str);
    dispose_regexps();

    CU_ASSERT_TRUE(result);
    free_all_generations();
}

void ehlo_incorrect_test()
{
    const char *input_str = "HAILO [127.0.0.1]";

    compile_regexps();
    int result = is_ehlo_command(input_str);
    dispose_regexps();

    CU_ASSERT_FALSE(result);
    free_all_generations();
}
