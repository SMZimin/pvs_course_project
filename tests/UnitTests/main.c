#include <stdio.h>
#include <CUnit/Basic.h>

#include "memory_manager_test.h"
#include "smtp_regexp_test.h"

int init_suite(void) { return 0; }
int clean_suite(void) { return 0; }

int main(void)
{
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    CU_pSuite mem_manager_suite = CU_add_suite("Memory manager test suite", init_suite, clean_suite);
    if (!mem_manager_suite)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (!CU_add_test(mem_manager_suite, "allocation_test", allocation_test) ||
            !CU_add_test(mem_manager_suite, "free_generation_usual_test", free_generation_usual_test) ||
            !CU_add_test(mem_manager_suite, "free_generation_zero_test", free_generation_zero_test) ||
            !CU_add_test(mem_manager_suite, "free_all_generations_usual_test", free_all_generations_usual_test) ||
            !CU_add_test(mem_manager_suite, "free_all_generations_zero_test", free_all_generations_zero_test))
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_pSuite regexp_suite = CU_add_suite("Regular expressions test suite", init_suite, clean_suite);
    if (!regexp_suite)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (!CU_add_test(regexp_suite, "get_email_from_str_usual_test1", get_email_from_str_usual_test1) ||
            !CU_add_test(regexp_suite, "get_email_from_str_usual_test2", get_email_from_str_usual_test2) ||
            !CU_add_test(regexp_suite, "get_email_from_str_usual_test3", get_email_from_str_usual_test3) ||
            !CU_add_test(regexp_suite, "get_email_from_str_usual_test4", get_email_from_str_usual_test4) ||
            !CU_add_test(regexp_suite, "get_email_from_str_usual_test5", get_email_from_str_usual_test5) ||
            !CU_add_test(regexp_suite, "get_email_from_str_incorrect_test1", get_email_from_str_incorrect_test1) ||
            !CU_add_test(regexp_suite, "get_email_from_str_incorrect_test2", get_email_from_str_incorrect_test2) ||
            !CU_add_test(regexp_suite, "get_email_from_str_incorrect_test3", get_email_from_str_incorrect_test3) ||
            !CU_add_test(regexp_suite, "get_email_from_str_incorrect_test4", get_email_from_str_incorrect_test4) ||
            !CU_add_test(regexp_suite, "get_email_from_str_incorrect_test5", get_email_from_str_incorrect_test5) ||
            !CU_add_test(regexp_suite, "get_domain_from_str_usual_test1", get_domain_from_str_usual_test1) ||
            !CU_add_test(regexp_suite, "ehlo_usual_test", ehlo_usual_test) ||
            !CU_add_test(regexp_suite, "ehlo_down_case_test", ehlo_down_case_test) ||
            !CU_add_test(regexp_suite, "ehlo_incorrect_test", ehlo_incorrect_test))
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run all tests using the basic interface
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    printf("\n");
    CU_basic_show_failures(CU_get_failure_list());
    printf("\n\n");
    /*
    // Run all tests using the automated interface
    CU_automated_run_tests();
    CU_list_tests_to_file();

    // Run all tests using the console interface
    CU_console_run_tests();
    */
    /* Clean up registry and return */
    CU_cleanup_registry();
    return CU_get_error();
}

