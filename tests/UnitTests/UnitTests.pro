TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    memory_manager_test.c \
    smtp_regexp_test.c

HEADERS += \
    memory_manager_test.h \
    smtp_regexp_test.h

