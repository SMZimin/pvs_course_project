import smtplib
from email.parser import Parser

with open('mail1.txt') as fp1:
    headers1 = Parser().parse(fp1)
with open('mail1.txt') as f1:
    message1 = f1.read()
    
with open('mail2.txt') as fp2:
    headers2 = Parser().parse(fp2)
with open('mail2.txt') as f2:
    message2 = f2.read()
    
server = smtplib.SMTP('localhost', 2525)
server.set_debuglevel(1)
server.sendmail(headers1['from'], headers1['to'], message1)
server.sendmail(headers2['from'], headers2['to'], message2)
server.quit()
