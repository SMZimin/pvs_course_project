SERVER_PATH := src
UNIT_TESTS_PATH := tests/UnitTests
SYSTEM_TESTS_PATH := tests/SystemTests
REPORT_PATH := report

all: server tests report

server: 
	make -C $(SERVER_PATH)

tests: unit_tests system_tests

unit_tests: 
	make -C $(UNIT_TESTS_PATH)

.ONESHELL:
system_tests: 
	cd $(SYSTEM_TESTS_PATH)
	./run_test.sh
	cd ../..

report:
	make -C $(REPORT_PATH)


clean:
	make -C $(SERVER_PATH) clean
	make -C $(UNIT_TESTS_PATH) clean
	make -C $(SYSTEM_TESTS_PATH) clean
	make -C $(REPORT_PATH) clean
	
.PHONY: all server tests unit_tests system_tests report
