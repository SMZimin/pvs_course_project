#include "sockets.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#include "privileges.h"
#include "app_assert.h"
#include "logging.h"
#include "error_codes.h"
#include "memory_manager.h"

int make_socket_non_blocking(int socketfd)
{
    return fcntl(socketfd, F_SETFL, fcntl(socketfd, F_GETFL, 0) | O_NONBLOCK);
}

int create_listening_socket(int port)
{
    char logbuf[128];

    int listen_socketfd = socket(AF_INET, SOCK_STREAM, 0);
    app_assert(listen_socketfd >= 0, "ERROR opening socket", OPEN_SOCKET_ERROR);
    sprintf(logbuf, "Listening socket %d opened", listen_socketfd);
    log_debug(logbuf);

    int yes = 1;
    app_assert(setsockopt(listen_socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) != -1, "ERROR on setsockopt", SETSCOKOPT_ERROR);

    struct sockaddr_in serv_addr;
    memset(&serv_addr, '\0', sizeof(struct sockaddr_in));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port);

    if (bind(listen_socketfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        close(listen_socketfd);
        sprintf(logbuf, "ERROR on binding listening socket %d to port %d", listen_socketfd, port);
        app_error(logbuf, BINDING_SOCKET_ERROR);
    }
    sprintf(logbuf, "Listening socket %d binded to port %d", listen_socketfd, port);
    log_debug(logbuf);

    if (port < 1024)
    {
        if (!drop_root_privileges())
            log_info("Root privileges successfully dropped");
        else
        {
            close(listen_socketfd);
            app_error("Can't drop root privileges!", DROP_ROOT_PRIVILEGES_ERROR);
        }
    }

    app_assert(make_socket_non_blocking(listen_socketfd) == 0, "Can't make listening socket non-blocking", MAKING_SOCKET_NON_BLOCKING_ERROR);

    listen(listen_socketfd, 5);
    log_debug("Started listening for connections");

    return listen_socketfd;
}

struct socket_client_info *accept_new_client(int listen_socket)
{
    struct sockaddr_in cli_addr;
    memset(&cli_addr, '\0', sizeof(struct sockaddr_in));
    socklen_t clilen;
    clilen = sizeof(cli_addr);

    int socketfd = accept(listen_socket, (struct sockaddr *) &cli_addr, &clilen);

    struct socket_client_info *client_info = NULL;
    char logbuf[128];

    if (socketfd >= 0)
    {
        sprintf(logbuf, "Working socket %d opened", socketfd);
        log_debug(logbuf);

        client_info = allocate_memory(sizeof(struct socket_client_info), socketfd);
        client_info->socket = socketfd;
        client_info->ip = (char *) inet_ntop(AF_INET, &cli_addr.sin_addr, allocate_memory(INET_ADDRSTRLEN + 1, socketfd), INET_ADDRSTRLEN + 1);

        if (make_socket_non_blocking(socketfd))
        {
            sprintf(logbuf, "Can't make working socket %d non-blocking", socketfd);
            log_error(logbuf);
        }
    }
    else
        log_debug("Opening working socket failed");


    return client_info;
}

void close_listening_socket(int listen_socket)
{
    close(listen_socket);
    log_debug("Listening socket closed");
}

int check_domain_ip(char *domain, char *ip)
{
    int res = 0;
    char buff[17];
    memset(buff, '\0', sizeof(buff));

    struct hostent *he = gethostbyname(domain);
    if (he)
    {
        struct in_addr **addr_list = (struct in_addr **) he->h_addr_list;

        for(int i = 0; (addr_list[i] != NULL) && !res; i++)
        {
            strcpy(buff, inet_ntoa(*addr_list[i]) );
            if (!strncmp(buff, ip, sizeof(buff)))
                res = 1;
        }
    }

    return res;
}
