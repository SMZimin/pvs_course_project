#include "signals.h"

#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "logging.h"
#include "app_assert.h"
#include "error_codes.h"

void block_signals()
{
    sigset_t mask;
    sigfillset(&mask);
    app_assert(sigprocmask(SIG_SETMASK, &mask, NULL) == 0, "Can't block signals.", BLOCK_SIGNALS_ERROR);
    log_debug("All signals blocked.");
}

void set_handler_to_signals(void (*signal_handler)(int signal))
{
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = signal_handler;
    action.sa_flags = 0;
    sigemptyset(&action.sa_mask);

    app_assert(sigaction(SIGINT, &action, NULL) == 0, "Can't set handler for SIGINT.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGALRM, &action, NULL) == 0, "Can't set handler for SIGALRM.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGABRT, &action, NULL) == 0, "Can't set handler for SIGABRT.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGQUIT, &action, NULL) == 0, "Can't set handler for SIGQUIT.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGTERM, &action, NULL) == 0, "Can't set handler for SIGTERM.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGFPE, &action, NULL) == 0, "Can't set handler for SIGFPE.", SIGNAL_HANDLER_SETTING_ERROR);
    app_assert(sigaction(SIGHUP, &action, NULL) == 0, "Can't set handler for SIGHUP.", SIGNAL_HANDLER_SETTING_ERROR);

    log_debug("Handler for signals has been set.");
}

void set_alarm(int seconds)
{
    char logbuf[128];
    alarm(seconds);
    sprintf(logbuf, "Alarm() set on %d seconds.", seconds);
    log_info(logbuf);
}
