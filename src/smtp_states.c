#include "smtp_states.h"

#include <string.h>
#include <unistd.h>

#include "logging.h"
#include "regexp_functions.h"
#include "sockets.h"
#include "maildir.h"
#include "memory_manager.h"

#define LOGBUF_SIZE 1236
#define READBUFF_SIZE 1024

// states
int initial(struct client_state *client);
int helo_response(struct client_state *client);
int ehlo_response(struct client_state *client);
int incorrect_parameters_response(struct client_state *client);
int unrecognised_command_response(struct client_state *client);
int vrfy_response_initial(struct client_state *client);
int quit_response(struct client_state *client);
int mail(struct client_state *client);
int mail_response(struct client_state *client);
int vrfy_response_mail(struct client_state *client);
int rset_response(struct client_state *client);
int rcpt(struct client_state *client);
int rcpt_response(struct client_state *client);
int data_response(struct client_state *client);
int vrfy_response_rcpt(struct client_state *client);
int data(struct client_state *client);
int transaction_response(struct client_state *client);
int transaction_failed_response(struct client_state *client);

// reusful functions
void helo_inner_handler(char *logbuf, const char *readbuf, struct client_state *client);
void ehlo_inner_handler(char *logbuf, const char *readbuf, struct client_state *client);
void quit_inner_handler(char *logbuf, struct client_state *client);
void unrecognised_command_inner_handler(char *logbuf, struct client_state *client);
void rset_inner_handler(char *logbuf, struct client_state *client);
void append_to_new_buffer(struct client_state *client, const char *text_to_append);

int connect_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "connect_response for socket \"%d\"", client->socket);
    log_debug(logbuf);
    sprintf(logbuf, "New client on socket \"%d\".", client->socket);
    log_info(logbuf);

    int result = CLIENT_CONTINUE;
    char *resp = "220 \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &initial;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in connect_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        log_debug("failed_write in connect_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
        result = CLIENT_END;
    }

    return result;
}

int initial(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "initial for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char readbuf[READBUFF_SIZE];
    memset(readbuf, '\0', READBUFF_SIZE);

    int n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n <= 0)
    {
        client->next_handler = &unrecognised_command_response;
        client->next_handler_type = WRITE_HANDLER;
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_error(logbuf);
    }
    else
    {
        snprintf(logbuf, LOGBUF_SIZE - 1, "\"%d\" bytes readed: \"%s\"", n, readbuf);
        log_debug(logbuf);

        if (is_helo_command(readbuf))
        {
            helo_inner_handler(logbuf, readbuf, client);
        }
        else if (is_ehlo_command(readbuf))
        {
            ehlo_inner_handler(logbuf, readbuf, client);
        }
        else if (is_vrfy_command(readbuf))
        {
            sprintf(logbuf, "VRFY command on socket \"%d\" received", client->socket);
            log_info(logbuf);
            client->next_handler = &vrfy_response_initial;
            client->next_handler_type = WRITE_HANDLER;
        }
        else if (is_quit_command(readbuf))
        {
            quit_inner_handler(logbuf, client);
        }
        else
        {
            unrecognised_command_inner_handler(logbuf, client);
        }
    }

    return CLIENT_CONTINUE;
}

int helo_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "helo_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 ZiminServer SMTP server say Hello \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &mail;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in helo_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in helo_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int ehlo_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "ehlo_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 ZiminServer SMTP server say Hello \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &mail;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in ehlo_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in ehlo_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int incorrect_parameters_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "incorrect_parameters_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "501 Syntax error in parameters or arguments! \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);

    client->next_handler = &close_connection;
    client->next_handler_type = UNIVERSAL_HANDLER;

    if (n == resp_len)
    {
        log_debug("successfull_write in incorrect_parameters_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        log_debug("failed_write in incorrect_parameters_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int unrecognised_command_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "unrecognised_command_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "500 Syntax error, command unrecognised! \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);

    client->next_handler = &close_connection;
    client->next_handler_type = UNIVERSAL_HANDLER;

    if (n == resp_len)
    {
        log_debug("successfull_write in unrecognised_command_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        log_debug("failed_write in unrecognised_command_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int vrfy_response_initial(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "vrfy_response_initial for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "252 Can not VRFY user, but will accept message and attempt delivery. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &initial;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in vrfy_response_initial");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in vrfy_response_initial");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int quit_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "quit_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "221 Closing transmission channel. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        log_debug("successfull_write in quit_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        log_debug("failed_write in quit_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    sprintf(logbuf, "Connection with client on socket \"%d\" closed.", client->socket);
    log_info(logbuf);

    return CLIENT_END;
}

int mail(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "mail for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char readbuf[READBUFF_SIZE];
    memset(readbuf, '\0', READBUFF_SIZE);

    int n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n <= 0)
    {
        client->next_handler = &unrecognised_command_response;
        client->next_handler_type = WRITE_HANDLER;
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_error(logbuf);
    }
    else
    {
        snprintf(logbuf, LOGBUF_SIZE - 1, "\"%d\" bytes readed: \"%s\"", n, readbuf);
        log_debug(logbuf);

        if (is_mail_command(readbuf))
        {
            sprintf(logbuf, "MAIL command on socket \"%d\" received", client->socket);
            log_info(logbuf);

            char *email = get_email_from_str(readbuf);
            if (email)
            {
                sprintf(logbuf, "Sender's email \"%s\" on socket \"%d\" extracted.", email, client->socket);
                log_info(logbuf);

                client->mail_id = create_uuid(client->socket);
                client->mail_buffer = NULL;
                client->mail_buffer_size = 0;

                char *text = allocate_memory(256, LOCAL);
                snprintf(text, 255, "ZiminServer_MESSAGE_ID: %s\nZiminServer_FROM: %s",
                         client->mail_id, email);
                append_to_new_buffer(client, text);
                log_debug(client->mail_buffer);

                client->next_handler = &mail_response;
                client->next_handler_type = WRITE_HANDLER;
            }
            else
            {
                log_info("Can not extract email from MAIL command.");
                client->next_handler = &incorrect_parameters_response;
                client->next_handler_type = WRITE_HANDLER;
            }
        }
        else if (is_vrfy_command(readbuf))
        {
            sprintf(logbuf, "VRFY command on socket \"%d\" received", client->socket);
            log_info(logbuf);
            client->next_handler = &vrfy_response_mail;
            client->next_handler_type = WRITE_HANDLER;
        }
        else if (is_rset_command(readbuf))
        {
            rset_inner_handler(logbuf, client);
        }
        else if (is_quit_command(readbuf))
        {
            quit_inner_handler(logbuf, client);
        }
        else if (is_helo_command(readbuf))
        {
            helo_inner_handler(logbuf, readbuf, client);
        }
        else if (is_ehlo_command(readbuf))
        {
            ehlo_inner_handler(logbuf, readbuf, client);
        }
        else
        {
            unrecognised_command_inner_handler(logbuf, client);
        }
    }

    return CLIENT_CONTINUE;
}

int close_connection(struct client_state *client)
{
    int result = CLIENT_END;

    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "close_connection for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char readbuf[READBUFF_SIZE];
    memset(readbuf, '\0', READBUFF_SIZE);

    int n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n > 0)
    {
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_debug(logbuf);
    }

    char *resp = "554 Transaction failed. Closing channel. \r\n";
    int resp_len = strlen(resp);
    n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        log_debug("successfull_write in close_connection");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in close_connection");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    // QUIT expected
    n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n > 0)
    {
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_debug(logbuf);

        if (is_quit_command(readbuf))
        {
            quit_inner_handler(logbuf, client);
            result = CLIENT_CONTINUE;
        }
    }

    sprintf(logbuf, "Closing connection for socket \"%d\"", client->socket);
    log_info(logbuf);
    return result;
}

int mail_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "mail_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 Requested mail action okay, completed. Sender accepted. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &rcpt;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in mail_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in mail_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int vrfy_response_mail(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "vrfy_response_mail for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "252 Can not VRFY user, but will accept message and attempt delivery. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &mail;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in vrfy_response_mail");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in vrfy_response_mail");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int rset_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "rset_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 ZiminServer SMTP server will reset all send action. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &mail;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in rset_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in rset_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int rcpt(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "rcpt for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char readbuf[READBUFF_SIZE];
    memset(readbuf, '\0', READBUFF_SIZE);

    int n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n <= 0)
    {
        client->next_handler = &unrecognised_command_response;
        client->next_handler_type = WRITE_HANDLER;
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_error(logbuf);
    }
    else
    {
        snprintf(logbuf, LOGBUF_SIZE - 1, "\"%d\" bytes readed: \"%s\"", n, readbuf);
        log_debug(logbuf);

        if (is_rcpt_command(readbuf))
        {
            sprintf(logbuf, "RCPT command on socket \"%d\" received", client->socket);
            log_info(logbuf);

            char *email = get_email_from_str(readbuf);
            if (email)
            {
                sprintf(logbuf, "Recipient's email \"%s\" on socket \"%d\" extracted.", email, client->socket);
                log_info(logbuf);

                char *text = allocate_memory(256, LOCAL);
                snprintf(text, 255, "\nZiminServer_TO: %s", email);
                append_to_new_buffer(client, text);
                log_debug(client->mail_buffer);

                client->next_handler = &rcpt_response;
                client->next_handler_type = WRITE_HANDLER;
            }
            else
            {
                log_info("Can not extract email from RCPT command.");
                client->next_handler = &incorrect_parameters_response;
                client->next_handler_type = WRITE_HANDLER;
            }
        }
        else if (is_vrfy_command(readbuf))
        {
            sprintf(logbuf, "VRFY command on socket \"%d\" received", client->socket);
            log_info(logbuf);
            client->next_handler = &vrfy_response_rcpt;
            client->next_handler_type = WRITE_HANDLER;
        }
        else if (is_data_command(readbuf))
        {
            sprintf(logbuf, "DATA command on socket \"%d\" received", client->socket);
            log_info(logbuf);
            append_to_new_buffer(client, "\n\n");
            client->next_handler = &data_response;
            client->next_handler_type = WRITE_HANDLER;
        }
        else if (is_rset_command(readbuf))
        {
            rset_inner_handler(logbuf, client);
        }
        else if (is_quit_command(readbuf))
        {
            quit_inner_handler(logbuf, client);
        }
        else if (is_helo_command(readbuf))
        {
            helo_inner_handler(logbuf, readbuf, client);
        }
        else if (is_ehlo_command(readbuf))
        {
            ehlo_inner_handler(logbuf, readbuf, client);
        }
        else
        {
            unrecognised_command_inner_handler(logbuf, client);
        }
    }

    return CLIENT_CONTINUE;
}

int rcpt_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "rcpt_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 Requested mail action okay, completed. Recipient accepted. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &rcpt;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in rcpt_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in rcpt_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int data_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "data_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "354 Start mail input; end with <CRLF>.<CRLF> \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &data;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in data_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in data_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int vrfy_response_rcpt(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "vrfy_response_rcpt for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "252 Can not VRFY user, but will accept message and attempt delivery. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &rcpt;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in vrfy_response_rcpt");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in vrfy_response_rcpt");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int data(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "data for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char readbuf[READBUFF_SIZE];
    memset(readbuf, '\0', READBUFF_SIZE);

    int n = read(client->socket, readbuf, READBUFF_SIZE - 1);
    if (n <= 0)
    {
        client->next_handler = &unrecognised_command_response;
        client->next_handler_type = WRITE_HANDLER;
        sprintf(logbuf, "read() returned \"%d\"", n);
        log_error(logbuf);
    }
    else
    {
        snprintf(logbuf, LOGBUF_SIZE - 1, "\"%d\" bytes readed: \"%s\"", n, readbuf);
        log_debug(logbuf);

        append_to_new_buffer(client, readbuf);

        if (strstr(client->mail_buffer, "\r\n.\r\n"))
        {
            int error = 0;
            char *tmp_filename = get_tmp_filename(client->maildir, client->mail_id);
            FILE *fp_tmp;
            fp_tmp = fopen(tmp_filename, "a");
            if (!fp_tmp)
            {
                error = 1;
                sprintf(logbuf, "Can't create file \"%s\"", tmp_filename);
                log_error(logbuf);
            }
            if (!error && fputs(client->mail_buffer, fp_tmp) < 0)
            {
                error = 1;
                sprintf(logbuf, "Error during writing to file \"%s\"", tmp_filename);
                log_error(logbuf);
                fclose(fp_tmp);
            }
            if (!error && fclose(fp_tmp))
            {
                error = 1;
                sprintf(logbuf, "Error during closing file \"%s\"", tmp_filename);
                log_error(logbuf);
            }

            char *new_filename = get_new_filename(client->maildir, client->mail_id);
            char *command_buf = allocate_memory(512, LOCAL);
            sprintf(command_buf, "cp %s %s", tmp_filename, new_filename);
            log_debug(command_buf);
            if (!error && system(command_buf) == -1)
            {
                error = 1;
                sprintf(logbuf, "Error during copy file \"%s\" to file \"%s\"", tmp_filename, new_filename);
                log_error(logbuf);
            }

            sprintf(command_buf, "rm %s", tmp_filename);
            log_debug(command_buf);
            if (!error && system(command_buf) == -1)
            {
                sprintf(logbuf, "Error during removing file \"%s\"", tmp_filename);
                log_error(logbuf);
            }

            if (!error)
            {
                client->next_handler = &transaction_response;
                client->next_handler_type = WRITE_HANDLER;
                char *logmes = allocate_memory(client->mail_buffer_size + 64, LOCAL);
                sprintf(logmes, "Mail on socket \"%d\" successfully received:\n\"%s\"", client->socket, client->mail_buffer);
                log_info(logmes);
            }
            else
            {
                client->next_handler = &transaction_failed_response;
                client->next_handler_type = WRITE_HANDLER;
            }
        }
        else
        {
            client->next_handler = &data;
            client->next_handler_type = READ_HANDLER;
        }
    }

    return CLIENT_CONTINUE;
}

int transaction_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "transaction_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "250 Requested mail action okay, completed. Recipient accepted. \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &mail;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in transaction_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in transaction_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

int transaction_failed_response(struct client_state *client)
{
    char logbuf[LOGBUF_SIZE];
    sprintf(logbuf, "transaction_failed_response for socket \"%d\"", client->socket);
    log_debug(logbuf);

    char *resp = "554 Transaction failed \r\n";
    int resp_len = strlen(resp);
    int n = write(client->socket, resp, resp_len);
    if (n == resp_len)
    {
        client->next_handler = &close_connection;
        client->next_handler_type = READ_HANDLER;
        log_debug("successfull_write in transaction_failed_response");
        sprintf(logbuf, "Server replied \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }
    else
    {
        client->next_handler = &close_connection;
        client->next_handler_type = UNIVERSAL_HANDLER;
        log_debug("failed_write in transaction_failed_response");
        sprintf(logbuf, "Server failed to reply \"%s\" on socket \"%d\".", resp, client->socket);
        log_debug(logbuf);
    }

    return CLIENT_CONTINUE;
}

//---------------- reuseful funcs

void helo_inner_handler(char *logbuf, const char *readbuf, struct client_state *client)
{
    sprintf(logbuf, "HELO command on socket \"%d\" received", client->socket);
    log_info(logbuf);

    char *domain = get_domain_from_str(readbuf);
    if (domain)
    {
        if (check_domain_ip(domain, client->ip))
        {
            sprintf(logbuf, "Client's IP \"%s\" on socket \"%d\" match it's domain name \"%s\".",
                    client->ip, client->socket, domain);
        }
        else
        {
            sprintf(logbuf, "Client's IP \"%s\" on socket \"%d\" doesn't match it's domain name \"%s\"!",
                    client->ip, client->socket, domain);
        }
        log_info(logbuf);

        client->next_handler = &helo_response;
        client->next_handler_type = WRITE_HANDLER;
    }
    else
    {
        log_info("Can not extract domain name from HELO command.");
        client->next_handler = &incorrect_parameters_response;
        client->next_handler_type = WRITE_HANDLER;
    }
}

void ehlo_inner_handler(char *logbuf, const char *readbuf, struct client_state *client)
{
    sprintf(logbuf, "EHLO command on socket \"%d\" received", client->socket);
    log_info(logbuf);

    char *domain = get_domain_from_str(readbuf);
    if (domain)
    {
        if (check_domain_ip(domain, client->ip))
        {
            sprintf(logbuf, "Client's IP \"%s\" on socket \"%d\" match it's domain name \"%s\".",
                    client->ip, client->socket, domain);
        }
        else
        {
            sprintf(logbuf, "Client's IP \"%s\" on socket \"%d\" doesn't match it's domain name \"%s\"!",
                    client->ip, client->socket, domain);
        }
        log_info(logbuf);

        client->next_handler = &ehlo_response;
        client->next_handler_type = WRITE_HANDLER;
    }
    else
    {
        log_info("Can not extract domain name from EHLO command.");
        client->next_handler = &incorrect_parameters_response;
        client->next_handler_type = WRITE_HANDLER;
    }
}

void quit_inner_handler(char *logbuf, struct client_state *client)
{
    sprintf(logbuf, "QUIT command on socket \"%d\" received", client->socket);
    log_info(logbuf);
    client->next_handler = &quit_response;
    client->next_handler_type = WRITE_HANDLER;
}

void unrecognised_command_inner_handler(char *logbuf, struct client_state *client)
{
    sprintf(logbuf, "Unrecognized command on socket \"%d\" received", client->socket);
    log_info(logbuf);
    client->next_handler = &unrecognised_command_response;
    client->next_handler_type = WRITE_HANDLER;
}

void rset_inner_handler(char *logbuf, struct client_state *client)
{
    sprintf(logbuf, "RSET command on socket \"%d\" received", client->socket);
    log_info(logbuf);
    client->next_handler = &rset_response;
    client->next_handler_type = WRITE_HANDLER;
}

void append_to_new_buffer(struct client_state *client, const char *text_to_append)
{
    int new_buf_size = client->mail_buffer_size + strlen(text_to_append) + 1;
    char *new_buf = allocate_memory(new_buf_size, client->socket);

    if (client->mail_buffer == NULL)
        strcpy(new_buf, text_to_append);
    else
        sprintf(new_buf, "%s%s", client->mail_buffer, text_to_append);

    client->mail_buffer = new_buf;
    client->mail_buffer_size = new_buf_size;
}
