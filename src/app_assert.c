#include "app_assert.h"

#include "logging.h"
#include "memory_manager.h"

void app_assert(int assert_condition, const char *fail_msg, int error_code)
{
    if (!assert_condition)
        app_error(fail_msg, error_code);
}

void app_error(const char *fail_msg, int error_code)
{
    log_error(fail_msg);
    free_all_generations();
    exit(error_code);
}
