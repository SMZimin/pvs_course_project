#include "clients.h"

#include <string.h>

#include "memory_manager.h"
#include "utils.h"

struct client_state *create_client_state(int socket, char *ip, const char *maildir, int (*init_handler)(struct client_state *))
{
    struct client_state *client = allocate_memory(sizeof(struct client_state), socket);
    memset(client, '\0', sizeof(struct client_state));
    client->socket = socket;
    client->ip = ip;
    gettimeofday(&client->last_activity_time, NULL);
    client->mail_buffer = NULL;
    client->mail_buffer_size = 0;
    client->maildir = maildir;
    client->mail_id = NULL;
    client->next_handler = init_handler;
    client->next_handler_type = WRITE_HANDLER;

    return client;
}

fd_set get_rset_from_clients(struct clients_list *clients)
{
    fd_set rset;
    FD_ZERO(&rset);
    struct client_state *cur_client;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        if (cur_client->next_handler_type != WRITE_HANDLER)
            FD_SET(cur_client->socket, &rset);
    }
    return rset;
}

fd_set get_wset_from_clients(struct clients_list *clients)
{
    fd_set wset;
    FD_ZERO(&wset);
    struct client_state *cur_client;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        if (cur_client->next_handler_type != READ_HANDLER)
            FD_SET(cur_client->socket, &wset);
    }
    return wset;
}

int get_max_socket(struct clients_list *clients)
{
    int max_socket = -1;
    struct client_state *cur_client;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        if (cur_client->socket > max_socket)
            max_socket = cur_client->socket;
    }
    return max_socket;
}

int get_clients_count(struct clients_list *clients)
{
    int count = 0;
    struct client_state *cur_client;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        count++;
    }

    return count;
}

struct client_state *get_oldest_ready_client(struct clients_list *clients, fd_set *rset, fd_set *wset)
{
    struct client_state *oldest_client = NULL;
    if (!LIST_EMPTY(clients))
    {
        oldest_client = LIST_FIRST(clients);
        struct client_state *cur_client;
        LIST_FOREACH(cur_client, clients, pointers)
        {
            if ((FD_ISSET(cur_client->socket, rset) || FD_ISSET(cur_client->socket, wset))
                    && timercmp(&oldest_client->last_activity_time, &cur_client->last_activity_time, >))
                oldest_client = cur_client;
        }
    }
    return oldest_client;
}

void finish_client(struct client_state *client, int (*finishing_handler)(struct client_state *))
{
    client->next_handler = finishing_handler;
    client->next_handler(client);
}

void finish_all_clients(struct clients_list *clients, int (*finishing_handler)(struct client_state *))
{
    struct client_state *cur_client;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        finish_client(cur_client, finishing_handler);
    }
}

void remove_dead_clients(struct clients_list *clients, int client_max_lifetime_ms, int (*finishing_handler)(struct client_state *))
{
    struct timeval max_lifetime = timeval_from_milliseconds(client_max_lifetime_ms);
    struct timeval now, difference;
    gettimeofday(&now, NULL);

    struct client_state *cur_client;
    int generation_to_free = VOID;
    LIST_FOREACH(cur_client, clients, pointers)
    {
        timersub(&now, &cur_client->last_activity_time, &difference);
        if (timercmp(&difference, &max_lifetime, >))
        {
            if (generation_to_free != VOID) // have to free entry only on next iteration to not crash LIST_FOREACH work
            {
                free_generation(generation_to_free);
                generation_to_free = VOID;
            }

            generation_to_free = cur_client->socket;
            finish_client(cur_client, finishing_handler);
            LIST_REMOVE(cur_client, pointers);
        }
    }

    if (generation_to_free != VOID)
        free_generation(generation_to_free);
}
