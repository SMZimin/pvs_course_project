#ifndef APP_ASSERT_H
#define APP_ASSERT_H

void app_assert(int assert_condition, const char *fail_msg, int error_code);
void app_error(const char *fail_msg, int error_code);

#endif // APP_ASSERT_H
