TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    memory_manager.c \
    smtp_server.c \
    privileges.c \
    sockets.c \
    clients.c \
    error_codes.c \
    app_assert.c \
    smtp_states.c \
    signals.c \
    utils.c \
    regexps.c \
    regexp_functions.c \
    maildir.c

HEADERS += \
    memory_manager.h \
    smtp_server.h \
    privileges.h \
    sockets.h \
    clients.h \
    error_codes.h \
    app_assert.h \
    smtp_states.h \
    signals.h \
    utils.h \
    regexps.h \
    regexp_functions.h \
    maildir.h

