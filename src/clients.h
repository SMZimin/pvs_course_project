#ifndef CLIENTS_H
#define CLIENTS_H

#include <sys/time.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <stdio.h>

#define WRITE_HANDLER 0
#define READ_HANDLER 1
#define UNIVERSAL_HANDLER -1

struct client_state
{
    int socket;                 // socket file descriptor of connection
    char *ip;                   // ip address of client
    struct timeval last_activity_time;  // time of last activity
    char *mail_buffer;          // text buffer with mail text
    int mail_buffer_size;       // size of mail buffer
    const char *maildir;        // Path to maildir directory
    char *mail_id;              // Id of mail presented as UUID
    int (*next_handler)(struct client_state *client);
    int next_handler_type;
    LIST_ENTRY(client_state) pointers;
};

LIST_HEAD(clients_list, client_state);

struct client_state *create_client_state(int socket, char *ip, const char *maildir, int (*init_handler)(struct client_state *));
fd_set get_rset_from_clients(struct clients_list *clients);
fd_set get_wset_from_clients(struct clients_list *clients);
int get_max_socket(struct clients_list *clients);
int get_clients_count(struct clients_list *clients);
struct client_state *get_oldest_ready_client(struct clients_list *clients, fd_set *rset, fd_set *wset);
void finish_all_clients(struct clients_list *clients, int (*finishing_handler)(struct client_state *));
void remove_dead_clients(struct clients_list *clients, int client_max_lifetime_ms, int (*finishing_handler)(struct client_state *));

#endif // CLIENTS_H
