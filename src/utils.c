#include "utils.h"

#include <string.h>

int max(int value1, int value2)
{
    return (value1 > value2) ? (value1) : (value2);
}

struct timeval timeval_from_milliseconds(int ms)
{
    struct timeval time;
    memset(&time, '\0', sizeof(struct timeval));

    time.tv_sec = ms / 1000;
    time.tv_usec = (ms % 1000) * 1000;

    return time;
}
