#ifndef LOGGING_H
#define LOGGING_H

#include <mqueue.h>

#define QUEUE_NAME "/log_queue"
#define LOG_MESSAGE_MAX_SIZE 1024

void init_logging(const char *logging_queue_name);
void free_logging();
char *create_log_message(const char *text, const char level);
void log_debug(const char *message);
void log_info(const char *message);
void log_error(const char *message);

#endif // LOGGING_H
