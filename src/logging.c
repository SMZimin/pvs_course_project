#include "logging.h"

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "memory_manager.h"

//#define DEBUG

static mqd_t logger_descriptor = -1;

void init_logging(const char *logging_queue_name)
{
    logger_descriptor = mq_open(logging_queue_name, O_WRONLY);
    if (logger_descriptor == -1)
    {
        printf("Error while opening logging message queue.\n");
    }
}

void free_logging()
{
    int close_res = mq_close(logger_descriptor);
    if (close_res == -1)
        printf("Error while closing logging message queue.\n");
}

char *create_log_message(const char *text, const char level)
{
    char time_buff[30];
    struct tm *sTm;

    time_t now = time(0);
    sTm = gmtime(&now);
    strftime(time_buff, sizeof(time_buff), "%d/%b/%Y:%H:%M:%S %z", sTm);

    char *log_message = allocate_memory(6 + strlen(time_buff) + strlen(text), LOCAL);
    snprintf(log_message, LOG_MESSAGE_MAX_SIZE, "%c [%s] %s", level, time_buff, text);

    return log_message;
}

void send_log_message(const char *message)
{
    if (logger_descriptor != -1)
    {
        char buffer[LOG_MESSAGE_MAX_SIZE + 1];
        memset(buffer, '\0', LOG_MESSAGE_MAX_SIZE + 1);
        strncpy(buffer, message, LOG_MESSAGE_MAX_SIZE);
        if (mq_send(logger_descriptor, buffer, LOG_MESSAGE_MAX_SIZE, 0) < 0)
            printf("Error during sending log message.\n");
    }
//    else
//        printf("Error during sending log message, because queue wasn't opened.\n");
}

void log_debug(const char *message)
{
#ifdef DEBUG
    printf("%s\n", message);
    send_log_message(create_log_message(message, 'D'));
#endif
}

void log_info(const char *message)
{
    printf("%s\n", message);
    send_log_message(create_log_message(message, 'I'));
}

void log_error(const char *message)
{
    printf("%s\n", message);
    send_log_message(create_log_message(message, 'E'));
}
