#include "regexp_functions.h"

#include <string.h>
#include <stdio.h>
#include <pcre.h>

#include "memory_manager.h"
#include "logging.h"
#include "app_assert.h"
#include "error_codes.h"
#include "regexps.h"

static pcre *re_helo = NULL;
static pcre *re_ehlo = NULL;
static pcre *re_mail = NULL;
static pcre *re_rcpt = NULL;
static pcre *re_data = NULL;
static pcre *re_rset = NULL;
static pcre *re_quit = NULL;
static pcre *re_vrfy = NULL;
static pcre *re_email = NULL;
static pcre *re_domain = NULL;

pcre *compile_regexp(const char *pattern, int options)
{
    const char *errort;
    int erroffset;
    return pcre_compile((char *) pattern, options, &errort, &erroffset, NULL);
}

void compile_regexps()
{
    re_helo = compile_regexp(HELO_PATTERN, PCRE_CASELESS);
    app_assert(re_helo != NULL, "Error of compiling HELO regexp.", REGEXP_COMPILING_ERROR);

    re_ehlo = compile_regexp(EHLO_PATTERN, PCRE_CASELESS);
    app_assert(re_ehlo != NULL, "Error of compiling EHLO regexp.", REGEXP_COMPILING_ERROR);

    re_mail = compile_regexp(MAIL_PATTERN, PCRE_CASELESS);
    app_assert(re_mail != NULL, "Error of compiling MAIL regexp.", REGEXP_COMPILING_ERROR);

    re_rcpt = compile_regexp(RCPT_PATTERN, PCRE_CASELESS);
    app_assert(re_rcpt != NULL, "Error of compiling RCPT regexp.", REGEXP_COMPILING_ERROR);

    re_data = compile_regexp(DATA_PATTERN, PCRE_CASELESS);
    app_assert(re_data != NULL, "Error of compiling DATA regexp.", REGEXP_COMPILING_ERROR);

    re_rset = compile_regexp(RSET_PATTERN, PCRE_CASELESS);
    app_assert(re_rset != NULL, "Error of compiling RSET regexp.", REGEXP_COMPILING_ERROR);

    re_quit = compile_regexp(QUIT_PATTERN, PCRE_CASELESS);
    app_assert(re_quit != NULL, "Error of compiling QUIT regexp.", REGEXP_COMPILING_ERROR);

    re_vrfy = compile_regexp(VRFY_PATTERN, PCRE_CASELESS);
    app_assert(re_vrfy != NULL, "Error of compiling VRFY regexp.", REGEXP_COMPILING_ERROR);

    re_email = compile_regexp(EMAIL_PATTERN, 0);
    app_assert(re_email != NULL, "Error of compiling EMAIL regexp.", REGEXP_COMPILING_ERROR);

    re_domain = compile_regexp(DOMAIN_PATTERN, 0);
    app_assert(re_domain != NULL, "Error of compiling DOMAIN regexp.", REGEXP_COMPILING_ERROR);

    log_debug("All regexps compiled.");
}

void dispose_regexps()
{
    pcre_free(re_helo);
    re_helo = NULL;

    pcre_free(re_ehlo);
    re_ehlo = NULL;

    pcre_free(re_mail);
    re_mail = NULL;

    pcre_free(re_rcpt);
    re_rcpt = NULL;

    pcre_free(re_data);
    re_data = NULL;

    pcre_free(re_rset);
    re_rset = NULL;

    pcre_free(re_quit);
    re_quit = NULL;

    pcre_free(re_vrfy);
    re_vrfy = NULL;

    pcre_free(re_email);
    re_email = NULL;

    pcre_free(re_domain);
    re_domain = NULL;

    log_debug("All regexps disposed.");
}

int regexp_match(const char *str, pcre *re)
{
    int ovector[30];
    memset(ovector, '\0', sizeof(ovector));
    int res = pcre_exec(re,  NULL, (char *) str, strlen(str), 0, 0, ovector, sizeof(ovector) / sizeof(int));
    return res < 0 ? 0 : res;
}

int is_helo_command(const char *str)
{
    return regexp_match(str, re_helo);
}

int is_ehlo_command(const char *str)
{
    return regexp_match(str, re_ehlo);
}

int is_mail_command(const char *str)
{
    return regexp_match(str, re_mail);
}

int is_rcpt_command(const char *str)
{
    return regexp_match(str, re_rcpt);
}

int is_data_command(const char *str)
{
    return regexp_match(str, re_data);
}

int is_rset_command(const char *str)
{
    return regexp_match(str, re_rset);
}

int is_quit_command(const char *str)
{
    return regexp_match(str, re_quit);
}

int is_vrfy_command(const char *str)
{
    return regexp_match(str, re_vrfy);
}

char *get_email_from_str(const char *str)
{
    int count = 0;
    int ovector[30];
    memset(ovector, '\0', sizeof(ovector));
    count = pcre_exec(re_email,  NULL, (char *) str, strlen(str), 0, 0, ovector, sizeof(ovector) / sizeof(int));

    char *email_str = NULL;
    if (count > 0)
    {
        int email_offset = ovector[2];
        int email_len = ovector[3] - email_offset;

        email_str = allocate_memory(email_len + 1, LOCAL);
        memmove(email_str, str + email_offset, email_len);
        email_str[email_len] = '\0';
    }

    return email_str;
}

char *get_domain_from_str(const char *str)
{
    int count = 0;
    int ovector[30];
    memset(ovector, '\0', sizeof(ovector));
    count = pcre_exec(re_domain,  NULL, (char *) str, strlen(str), 0, 0, ovector, sizeof(ovector) / sizeof(int));

    char *domain_str = NULL;
    if (count > 0)
    {
        int domain_offset = ovector[2];
        int domain_len = ovector[3] - domain_offset;

        domain_str = allocate_memory(domain_len + 1, LOCAL);
        memmove(domain_str, str + domain_offset, domain_len);
        domain_str[domain_len] = '\0';
    }

    return domain_str;
}
