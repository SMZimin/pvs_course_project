#ifndef ERROR_CODES_H
#define ERROR_CODES_H

#define INVALID_ARGS_NUMBER -1
#define OPEN_SOCKET_ERROR -2
#define SETSCOKOPT_ERROR -3
#define BINDING_SOCKET_ERROR -4
#define DROP_ROOT_PRIVILEGES_ERROR -5
#define REGEXP_COMPILING_ERROR -6
#define BLOCK_SIGNALS_ERROR -7
#define SIGNAL_HANDLER_SETTING_ERROR -8
#define MAKING_SOCKET_NON_BLOCKING_ERROR -9

#endif // ERROR_CODES_H
