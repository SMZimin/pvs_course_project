#ifndef REGEXPS_H
#define REGEXPS_H

#define HELO_PATTERN "^HELO\\s"
#define EHLO_PATTERN "^EHLO\\s"
#define MAIL_PATTERN "^MAIL\\sFROM:"
#define RCPT_PATTERN "^RCPT\\sTO"
#define DATA_PATTERN "^DATA\\s?\\n?$"
#define RSET_PATTERN "^RSET\\s?\\n?$"
#define QUIT_PATTERN "^QUIT\\s?\\n?$"
#define VRFY_PATTERN "^VRFY\\s"
#define EMAIL_PATTERN "<(([a-zA-Z]+[\\w\\_\\.\\-]*)@([a-zA-Z0-9\\.\\-]+)\\.([a-zA-Z]+))>"
#define DOMAIN_PATTERN "\\[(.+)\\]"

#endif // REGEXPS_H
