#include "file_logging.h"

#include <stdio.h>
#include <stdlib.h>

void write_log_to_file(const char *filename, const char *log)
{
    FILE *fp;
    fp = fopen(filename, "a");
    if (!fp)
    {
        printf("\nPANIC!!! CAN NOT OPEN FILE: %s\n", filename);      // panic!!!
        exit(1);
    }
    if (fprintf(fp, "%s\n", log) < 0)
        printf("Error during writing log to file: \"%s\"", log);
    fclose(fp);
}

