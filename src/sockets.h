#ifndef SOCKETS_H
#define SOCKETS_H

struct socket_client_info
{
    int socket;
    char *ip;
};

int create_listening_socket(int port);
struct socket_client_info *accept_new_client(int listen_socket);
void close_listening_socket(int listen_socket);
int check_domain_ip(char *domain, char *ip);

#endif // SOCKETS_H
