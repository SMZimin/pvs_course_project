#ifndef MAILDIR_H
#define MAILDIR_H

char *create_uuid(int generation);
char *get_tmp_filename(const char *maildir, const char *file_id);
char *get_new_filename(const char *maildir, const char *file_id);

#endif // MAILDIR_H
