#ifndef REGEXP_FUNCTIONS_H
#define REGEXP_FUNCTIONS_H

void compile_regexps();
void dispose_regexps();

int is_helo_command(const char *str);
int is_ehlo_command(const char *str);
int is_mail_command(const char *str);
int is_rcpt_command(const char *str);
int is_data_command(const char *str);
int is_rset_command(const char *str);
int is_quit_command(const char *str);
int is_vrfy_command(const char *str);

char *get_email_from_str(const char *str);
char *get_domain_from_str(const char *str);

#endif // REGEXP_FUNCTIONS_H
