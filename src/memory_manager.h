#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <stdlib.h>

#define LOCAL -1
#define GLOBAL -2
#define VOID -3

void *allocate_memory(size_t byte_count, int generation_number);
int free_generation(int generation_number);
int free_all_generations();

#endif // MEMORY_MANAGER_H

