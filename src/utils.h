#ifndef UTILS_H
#define UTILS_H

#include <sys/time.h>

int max(int value1, int value2);
struct timeval timeval_from_milliseconds(int ms);
char *create_uuid(int generation);

#endif // UTILS_H
