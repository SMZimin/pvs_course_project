#include "smtp_server.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/queue.h>

#include "logging.h"
#include "memory_manager.h"
#include "privileges.h"
#include "app_assert.h"
#include "sockets.h"
#include "clients.h"
#include "utils.h"
#include "signals.h"
#include "smtp_states.h"

static int must_stop = 0;

void signal_handler(int signal)
{
    must_stop = 1;

    char logbuf[64];
    sprintf(logbuf, "Process catched signal %d", signal);
    log_info(logbuf);
}

void run_smtp_server(const char *maildir, int port, int max_clients, int client_max_lifetime_ms, int server_lifetime_sec)
{  
    block_signals();
    set_handler_to_signals(&signal_handler);

    if (server_lifetime_sec > 0)
        set_alarm(server_lifetime_sec);

    int listen_socketfd = create_listening_socket(port);

    struct clients_list clients;
    LIST_INIT(&clients);

    while (!must_stop)
    {
        remove_dead_clients(&clients, client_max_lifetime_ms, &close_connection);

        fd_set rfds = get_rset_from_clients(&clients);
        fd_set wfds = get_wset_from_clients(&clients);
        FD_SET(listen_socketfd, &rfds);

        sigset_t zeromask;
        sigemptyset(&zeromask);

        int ready_sockets_count = pselect(max(listen_socketfd, get_max_socket(&clients)) + 1, &rfds, &wfds, NULL, NULL, &zeromask);
        if (ready_sockets_count <= 0)
        {
            must_stop = 1;
            char logbuf[128];
            sprintf(logbuf, "Function pselect() returned \"%d\". Server closing started.", ready_sockets_count);
            log_info(logbuf);
        }
        if (!must_stop)
        {
            if (FD_ISSET(listen_socketfd, &rfds) && get_clients_count(&clients) < max_clients)
            {
                struct socket_client_info *socket_info = accept_new_client(listen_socketfd);
                if (socket_info)
                {
                    struct client_state *client = create_client_state(socket_info->socket, socket_info->ip, maildir, &connect_response);
                    LIST_INSERT_HEAD(&clients, client, pointers);
                }
            }
            else
            {
                struct client_state *client = get_oldest_ready_client(&clients, &rfds, &wfds);
                if (client && client->next_handler(client) == CLIENT_END)
                {
                    LIST_REMOVE(client, pointers);
                    free_generation(client->socket);
                    client = NULL;
                }
            }
        }
        free_generation(LOCAL);
    }

    finish_all_clients(&clients, &close_connection);
    close_listening_socket(listen_socketfd);
}
