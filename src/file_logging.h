#ifndef FILE_LOGGING_H
#define FILE_LOGGING_H

void write_log_to_file(const char *filename, const char *log);

#endif // FILE_LOGGING_H
