#ifndef SMTP_STATES_H
#define SMTP_STATES_H

#include "clients.h"

#define CLIENT_CONTINUE 0
#define CLIENT_END 1

int connect_response(struct client_state *client);

int close_connection(struct client_state *client);

#endif // SMTP_STATES_H
