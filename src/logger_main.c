#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>

#include "logging.h"
#include "file_logging.h"

#define INVALID_ARGS_NUMBER -1
#define MQ_OPEN_ERROR -2
#define MESSAGE_RECEIVE_ERROR -3
#define MQ_CLOSE_ERROR -4
#define MQ_UNLIK_ERROR -5

static mqd_t logger_descriptor;
static char logger_q_name[LOG_MESSAGE_MAX_SIZE];
static char filename[LOG_MESSAGE_MAX_SIZE];

void free_resources()
{
    int result_code = 0;
    write_log_to_file(filename, create_log_message("Logger close command.", 'I'));
    if (mq_close(logger_descriptor) == -1)
    {
        write_log_to_file(filename, create_log_message("Error during mq_close().", 'E'));
        printf("Error during mq_close().\n");
        result_code = MQ_CLOSE_ERROR;
    }
    if (mq_unlink(logger_q_name) == -1)
    {
        write_log_to_file(filename, create_log_message("Error during mq_unlink().", 'E'));
        printf("Error mq_unlink().\n");
        result_code = MQ_UNLIK_ERROR;
    }

    write_log_to_file(filename, create_log_message("Logger closed.", 'I'));
    printf("Logger closed");
    exit(result_code);
}

void signal_handler(int signum)
{
    free_resources();
}

int main(int argc, char** argv)
{
    if (argc != 3)
    {
        printf("Usage: ./logger <logger_queue_name> <logger_file_name>\n");
        return INVALID_ARGS_NUMBER;
    }

    strcpy(logger_q_name, argv[1]);
    strcpy(filename, argv[2]);

    printf("Logger started\n");
    write_log_to_file(filename, create_log_message("Logger started.", 'I'));


    signal(SIGINT, signal_handler);
    struct mq_attr attr;
    char buffer[LOG_MESSAGE_MAX_SIZE + 1];

    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = LOG_MESSAGE_MAX_SIZE;
    attr.mq_curmsgs = 0;

    logger_descriptor = mq_open(logger_q_name, O_CREAT | O_RDONLY, 0644, &attr);
    if (logger_descriptor == -1)
    {
        printf("Error while opening mq\n");
        exit(MQ_OPEN_ERROR);
    }

    while (1)
    {
        ssize_t bytes_read;

        bytes_read = mq_receive(logger_descriptor, buffer, LOG_MESSAGE_MAX_SIZE, NULL);
        if (bytes_read < 0)
        {
            printf("Error while receiving message\n");
            exit(MESSAGE_RECEIVE_ERROR);
        }

        buffer[bytes_read] = '\0';
        printf("%s\n", buffer);
        write_log_to_file(filename, buffer);
    }

    free_resources();
    printf("Logger closed");

    return 0;
}

