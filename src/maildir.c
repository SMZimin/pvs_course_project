#include "maildir.h"

#include <uuid/uuid.h>
#include <string.h>
#include <stdio.h>

#include "memory_manager.h"

char *create_uuid(int generation)
{
    uuid_t uuid;
    uuid_generate(uuid);
    char *uuid_str = allocate_memory(37, generation);
    uuid_unparse(uuid, uuid_str);

    return uuid_str;
}

char *get_tmp_filename(const char *maildir, const char *file_id)
{
    char *tmp_filename = allocate_memory(strlen(maildir) + strlen(file_id) + 6, LOCAL);
    sprintf(tmp_filename, "%s/tmp/%s", maildir, file_id);

    return tmp_filename;
}

char *get_new_filename(const char *maildir, const char *file_id)
{
    char *new_filename = allocate_memory(strlen(maildir) + strlen(file_id) + 6, LOCAL);
    sprintf(new_filename, "%s/new/%s", maildir, file_id);

    return new_filename;
}
