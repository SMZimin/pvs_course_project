TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += logger_main.c \
    logging.c \
    file_logging.c

HEADERS += \
    logging.h \
    file_logging.h
