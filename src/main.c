#include <stdio.h>
#include <stdlib.h>

#include "logging.h"
#include "smtp_server.h"
#include "memory_manager.h"
#include "error_codes.h"
#include "regexp_functions.h"

int main(int argc, char** argv)
{
    if (argc != 7)
    {
        printf("Usage: server <logger_queue_name> <maildir_path> <port_number> <max_client_count> \
                <client_max_lifetime_ms> <server_lifetime_sec>\n");
        return INVALID_ARGS_NUMBER;
    }

    init_logging(argv[1]);
    compile_regexps();
    log_info("Server started.");

    run_smtp_server(argv[2], atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]));

    dispose_regexps();
    log_info("Server closed.");
    log_debug("All heap memory freed.");
    free_all_generations();
    free_logging();

    return 0;
}

