#ifndef SIGNALS_H
#define SIGNALS_H

void block_signals();
void set_handler_to_signals(void (*signal_handler)(int signal));
void set_alarm(int seconds);

#endif // SIGNALS_H
