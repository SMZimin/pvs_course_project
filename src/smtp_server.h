#ifndef SMTP_SERVER_H
#define SMTP_SERVER_H

void run_smtp_server(const char *maildir, int port, int max_clients, int client_max_lifetime_ms, int server_lifetime_sec);

#endif // SMTP_SERVER_H
